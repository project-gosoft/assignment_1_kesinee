package day1;

public class Lab13 {
	public static void main(String[] args) {
		// ข้อ 1 สร้าง array 2 มิติ แล้ว print ค่าออกมาทุกจำนวน
		int[][] myArray = { { 1, 2, 3 }, { 4, 5, 6, 7 }, { 8, 9, 10 } };
		/*
		 * for(int[] row : myArray) { for(int element : row) {
		 * System.out.println(element); } }
		 */

//		};
		
		
		// ข้อ 2 ดึงค่าใน array แล้วเอาค่าสุดท้ายของแต่ละแถวมาบวกกัน
		int sum = 0;
		for (int row1 = 0; row1 < myArray.length; row1++) { // row
			for (int element1 = 0; element1 < myArray[row1].length; element1++) { // column
				// System.out.println("element" +element1);
				// System.out.println("Array : " +myArray[row1].length);
				if (element1 == myArray[row1].length - 1) { // -1 เพื่อให้มันเอาตัวสุดท้ายของแถว
					// System.out.println(myArray[row1][element1]); // ต้องระบุทั้ง row & column ถึงจะสามารถบอกค่าสุดท้ายองแถวได้
					// ที่ได้มาจาก if
					sum += myArray[row1][element1];
					// sum2 = +myArray[row1][element1];
					// System.out.println("myArray" +myArray[row1][element1]);

					// System.out.println("sum2 : "+sum2);
				}
				// =+ ให้ค่าท่าซ้ายมีค่าเท่าขวา
			}
			System.out.println("sum :" + sum);
		}

	}
}
