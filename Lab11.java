package day1;

public class Lab11 {
	public static void main(String[] args) {
		int num1 = 21, num2 = 12;
		String hello = "Good morning";
		System.out.println(addition(num1,num2));
		sayHello(hello);
	}
	public static int addition(int a, int b) {
		return a + b;
	}
	
	public static void sayHello(String word) {
		System.out.println(word);
	}
}
