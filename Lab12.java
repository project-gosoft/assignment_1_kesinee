package day1;

public class Lab12 {
	public static void main(String[] args) {
		/*
		 * char[] myChar = {'h','e','l','l','o'}; String myWord = new String(myChar);
		 * System.out.println(myWord);
		 
		String myText = "Hello World";
		String myText1 = "hello world";
		String myText2 = "Hello World";
		if (myText.equals(myText1)) {
			System.out.println("equals");
		} else {
			System.out.println("not equal");
		}
		if (myText.equals(myText2)) {
			System.out.println("equals");
		} else {
			System.out.println("not equal");
		}*/
		String string1 = "You and Me", string2 = " you and me ";
		
		//ข้อ 1 เปรียบเทียบ
		if(string1.equals(string2)) {
			System.out.println("equals");
		}else {
			System.out.println("not equal");
		}
		
		//ข้อ 2 ค้นหาคำ
		String check = "and";
		System.out.println("check string1 : " + string1.contains(check)+ " check string2 : "+ string2.contains(check));
		
		//ข้อ 3 หาความยาว string
		
		System.out.println("length string1 : " + string1.length() + " length string2 : " + string2.length());
		
		//ข้อ 4 ตัดคำตำแหน่ง 1-4 ออก
		
		System.out.println(string1.substring(0).substring(5));
		
		//ข้อ 5 ตัดช่องว่าง
		
		System.out.println("before : " + string2);
		System.out.println("After : " + string2.trim());
		
		// ข้อ 6 เปลี่ยนเป็นพิมพ์ใหญ่ทั้งหมด
		
		System.out.println(string1.toUpperCase());
		
		// ข้อ 7 เป้นพิมพใหญ่หมด และลบช่องว่างออก
		
		System.out.println(string2.toUpperCase().trim());
	}
}
