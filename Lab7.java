package day1;

import java.util.Scanner;

public class Lab7 {
	public static void main(String[] args) {
		int count = 20;
		do {
			System.out.println("count : " + count);
			--count;
		} while (count >= 1); // มันจะ loop ต่อไปจนกว่าค่า count ที่ได้จะมีค่าน้อยกว่า 1

		// ข้อ 2 ตรวจว่าเลขที่รับมาเป็นคู่หรือคี่

		Scanner inputNum = new Scanner(System.in);
		int num;
		do {
			System.out.println("Plase enter your number : ");
			num = inputNum.nextInt();

			if (num % 2 == 0) {
				System.out.println("เลขที่คุณกรอก " + num + " เป็นเลขคู่");
			} else {
				System.out.println("เลขที่คุณกรอก " + num + " เป็นเลขคี่");
			}
		} while (num % 2 == 0);

	}
}
