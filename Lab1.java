package day1;

public class Lab1 {

	public static void main(String[] args) {
		// numberOffood = 100; "//" คือการ comment แบบ Single Line
		/*
		 *  numberOfType = 200; เป็นการ comment แบบ Multiple Line
		 */
		int num = 100;
		boolean myBoolean = false;
		String name = "Kesinee";
		System.out.println("This is a int : " + num);
		System.out.println("This is a Boolean : " + myBoolean);
		System.out.println("This is a String : " + name);
	}
}
