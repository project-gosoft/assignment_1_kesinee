package day1;

import java.util.ArrayList;

public class Lab6 {
	public static void main(String[] args) {
		// ข้อ1
		int i = 1;
		while (i <= 10) {
			System.out.println(i);
			i++;
		}

		// ข้อ2 หาผลรวมเลข 1 - 10 บวกทีละคู่แล้วจำค่าไว่บวกต่อ
		int sum = 0;
		int counter = 1;
		while (counter <= 10) {
			System.out.println(counter);
			sum = sum + counter;
			System.out.println("counter : " + counter);
			System.out.println("sum : " + sum);
			counter++;

		}
		System.out.println("total : " + sum);
		
		//ข้อ3 หาค่าระหว่าง 1-100 ที่หาร 12 ลงตัว หรือเหลือเศษ = 0
		// % = หารเอาเศษ มันคือ % 12 = 0
		// 1. วน loop ตั้งแต่ 1-100
		// 2. ถ้าตอนนั้น % 12 = 0 ให้ปริ้นออกมา
		
		int counter1 = 1;
		ArrayList<Integer> resultList = new ArrayList<Integer>(); //สร้าง arrayList มาเก็บค่าผลลัพธ์ที่ได้จากการหาร 12 ลงตัว
		while(counter1 <= 100) { // ให้มันวน loop 1-100
			if(counter1 % 12 == 0) { //เช็คเงื่อนไขว่า เมื่อหาร 12 แล้วเศษเหลือ 0 มั้ย
				resultList.add(counter1);
				System.out.println("หารด้วย 12 ลงตัว : " + counter1); //ปริ้นตัวที่หาร 12 เหลือเศษ 0 ออกมา
			}
			counter1++; //บวกค่า counter1 ไปเรื่อยๆ
		}
		for(Integer res : resultList) { // เขียนเช็คดูข้อมูลที่เก็บไว้ใน array ที่ชื่อว่า resultList สร้างตัวแปรชื่อ res มาเก็บค่าไว้
			System.out.println(res);
		}
		
		//ข้อ4 
		int myArray[] = {1,2,3,4,5};
		for(int count : myArray) {
			System.out.println("Counter : " + count);
		}
		
	}
}
