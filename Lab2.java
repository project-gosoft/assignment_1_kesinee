package day1;

public class Lab2 {
	public static void main(String[] args) {
		bark();
		primitive();
		//hello();
	}

	public static void bark() { //ข้อ 1
		String dogName = "Sam";
		System.out.println("The dog name = " + dogName + " bark");
	}

	public static void primitive() { //ข้อ 2
		float myFloat = 2.2f;
		int myInt = (int) myFloat;// Narrowing
		System.out.println("myFloat value is : " + myFloat);
		System.out.println("myInt value is : " + myInt);

		int myInt2 = 3;
		float myFloat2 = myInt2; // widening
		System.out.println("myInt2 value is : " + myInt2);
		System.out.println("myFloat2 value is : " + myFloat2);

		double myDouble = 2.2d;
		float myFloat3 = (float) myDouble;
		System.out.println("myDouble value is : " + myDouble);
		System.out.println("myFloat3 value is : " + myFloat3);

		char myChar = 'a';
		int myInt3 = myChar;
		System.out.println("myChar value is : " + myChar);
		System.out.println("myInt2 value is : " + myInt3);
	}

	/*public static void hello() { //ข้อ 3
		final String hello1 = "hello";
		//  = "world"; ฟ้อง error ไม่สามารถเปลี่ยนค่าได้
		System.out.println(hello1);
	}*/ 

}
